SKIP_SQUASH?=1
IMAGE = opsperator/gogs
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: demodb
demodb:
	@@docker rm -f testpostgres || echo true
	@@docker run --name testpostgres \
	    -e POSTGRESQL_USER=pguser \
	    -e POSTGRESQL_DATABASE=pgdb \
	    -e POSTGRESQL_PASSWORD=pgpassword \
	    -d docker.io/centos/postgresql-12-centos7:latest

.PHONY: demo
demo: demodb
	@@docker rm -f testgogs || echo true
	@@mkdir -p wip
	@@pgip=`docker inspect testpostgres | awk '/"IPAddress": "/{print $$2}' | head -1 | cut -d'"' -f2`; \
	echo "Working with postgres=$$pgip"; \
	sed -e "s|DBHOST|$$pgip|" \
	    -e "s|DBNAME|pgdb|" \
	    -e "s|DBUSER|pguser|" \
	    -e "s|DBPASS|pgpassword|" \
	    ci-config.ini >wip/app.ini
	@@sleep 10
	@@chmod 777 wip/*
	@@docker run --name testgogs \
	    --entrypoint /app/gogs/gogs \
            -e HOME=/data/gogs \
            -e SOCAT_LINK=false \
	    --user 1000:0 \
	    --tmpfs /app/gogs/log:rw,size=65536k \
	    --tmpfs /app/gogs/docker/s6:rw,size=65536k  \
	    --tmpfs /data/gogs:rw,size=65536k  \
	    -v `pwd`/wip:/data/gogs/conf \
	    -d $(IMAGE) \
	    web

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in configmap secret service deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "WORDPRESS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "WORDPRESS_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml \
	    | oc delete -f- || true
