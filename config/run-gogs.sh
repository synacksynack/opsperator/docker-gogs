#!/bin/sh
# based on https://raw.githubusercontent.com/gogs/gogs/main/docker/start.sh

create_socat_links() {
    USED_PORT="3000:22"
    while read -r NAME ADDR PORT; do
	if test -z "$NAME$ADDR$PORT"; then
	    continue
	elif echo $USED_PORT | grep -E "(^|:)$PORT($|:)" > /dev/null; then
	    echo "init:socat  | Can't bind linked container $NAME to localhost, port $PORT already in use" >&2
	else
	    SERV_FOLDER=/app/gogs/docker/s6/SOCAT_${NAME}_$PORT
	    mkdir -p "$SERV_FOLDER"
	    CMD="socat -ls TCP4-LISTEN:$PORT,fork,reuseaddr TCP4:$ADDR:$PORT"
	    echo -e "#!/bin/sh\nexec $CMD" >"$SERV_FOLDER"/run
	    chmod +x "$SERV_FOLDER"/run
	    USED_PORT="$USED_PORT:$PORT"
	    echo "init:socat  | Linked container $NAME will be binded to localhost on port $PORT" >&2
	fi
    done << EOT
    $(env | sed -En 's|(.*)_PORT_([0-9]+)_TCP=tcp://(.*):([0-9]+)|\1 \3 \4|p')
EOT
}

cleanup() {
    rm -rf "$(find /app/gogs/docker/s6/ -name 'event')"
    rm -rf /app/gogs/docker/s6/SOCAT_*
}

create_volume_subfolder() {
    chown -R "$USER:$USER" /data
    for f in /data/gogs/data /data/gogs/conf /data/gogs/log /data/git /data/ssh; do
	if ! test -d $f; then
	    gosu "$USER" mkdir -p $f
	fi
    done
}

setids() {
    export USER=git
    PUID=${PUID:-1000}
    PGID=${PGID:-1000}
    groupmod -o -g "$PGID" $USER
    usermod -o -u "$PUID" $USER
}

setids
cleanup
create_volume_subfolder

if ls /usr/local/share/ca-certificates/ 2>/dev/null | grep -E '(crt|pem)' >/dev/null; then
    echo "init:certs   | Updating Trusted CA Certificates"
    update-ca-certificates
fi

LINK=$(echo "$SOCAT_LINK" | tr '[:upper:]' '[:lower:]')
if [ "$LINK" = "false" ] || [ "$LINK" = "0" ]; then
    echo "init:socat  | Will not try to create socat links as requested" 1>&2
else
    create_socat_links
fi

CROND=$(echo "$RUN_CROND" | tr '[:upper:]' '[:lower:]')
if [ "$CROND" = "true" ] || [ "$CROND" = "1" ]; then
    echo "init:crond  | Cron Daemon (crond) will be run as requested by s6" 1>&2
    rm -f /app/gogs/docker/s6/crond/down
    /bin/sh /app/gogs/docker/runtime/backup-init.sh "$PUID"
else
    touch /app/gogs/docker/s6/crond/down
fi

if [ $# -gt 0 ];then
    exec "$@"
else
    exec /bin/s6-svscan /app/gogs/docker/s6/
fi
