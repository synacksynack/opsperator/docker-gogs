FROM docker.io/golang:alpine3.14 AS builder

ARG GOGS_VERSION=v0.12.10

RUN apk --no-cache --no-progress add --virtual \
	build-deps build-base git linux-pam-dev go-bindata \
    && mkdir -p /gogs.io/gogs \
    && git clone -b $GOGS_VERSION \
	https://github.com/gogs/gogs \
	/gogs.io/gogs \
    && cd /gogs.io/gogs \
    && make build TAGS="cert pam"

FROM docker.io/alpine:3.14

ARG GOGS_VERSION=v0.12.10

LABEL io.k8s.description="Gogs is a Go based Git Repository" \
      io.k8s.display-name="Gogs $GOGS_VERSION" \
      io.openshift.expose-services="3000:http,22:ssh" \
      io.openshift.tags="gogs" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-gogs" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$GOGS_VERSION"

RUN set -x \
    && if test `uname -m` = aarch64; then \
	export MYARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	export MYARCH=armhf; \
    else \
	export MYARCH=amd64; \
    fi \
    && wget -O /usr/sbin/gosu \
	https://github.com/tianon/gosu/releases/download/1.11/gosu-$MYARCH \
    && chmod +x /usr/sbin/gosu \
    && echo http://dl-2.alpinelinux.org/alpine/edge/community/ \
	>>/etc/apk/repositories \
    && apk --no-cache --no-progress add bash ca-certificates curl git \
	linux-pam openssh s6 shadow socat tzdata rsync

ENV GOGS_CUSTOM /data/gogs

WORKDIR /app/gogs

COPY --from=builder /gogs.io/gogs/gogs .
COPY --from=builder /gogs.io/gogs/docker ./docker
COPY config/run-gogs.sh /app/gogs/docker/

RUN cat /app/gogs/docker/nsswitch.conf >/etc/nsswitch.conf \
    && ./docker/finalize.sh \
    && mkdir -p /data /backup \
    && chown -R 1000:0 /data /backup /etc/ssl \
    && chmod -R g=u /data /backup /etc/ssl

VOLUME ["/data", "/backup"]
EXPOSE 22 3000
ENTRYPOINT ["/app/gogs/docker/run-gogs.sh"]
CMD ["/bin/s6-svscan", "/app/gogs/docker/s6/"]
USER 1000
